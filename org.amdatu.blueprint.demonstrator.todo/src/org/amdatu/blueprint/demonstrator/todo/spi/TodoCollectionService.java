package org.amdatu.blueprint.demonstrator.todo.spi;



import org.amdatu.blueprint.demonstrator.todo.api.Todo;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by brampouwelse on 1/16/17.
 */
public interface TodoCollectionService {

    Todo add(Todo todo);

    void update(Todo todo);

    void delete(String id);

    Stream<Todo> all();

    Optional<Todo> find(String id);
}
