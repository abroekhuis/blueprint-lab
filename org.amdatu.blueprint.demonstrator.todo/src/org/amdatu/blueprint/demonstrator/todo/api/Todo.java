package org.amdatu.blueprint.demonstrator.todo.api;


public class Todo {

    private String id;

    private String name;

    private boolean done;

    @Deprecated // Only for RS
    public Todo() {
    }

    public Todo(String id, String name, boolean done) {
        this.id = id;
        this.name = name;
        this.done = done;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isDone() {
        return done;
    }
}
