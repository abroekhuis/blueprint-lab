package org.amdatu.blueprint.demonstrator.todo.api;

import java.util.List;

public interface TodoService {

    List<Todo> list();

    Todo add(String name);

    void markDone(String id);

    void markNotDone(String id);

    void delete(String id);

}

