package org.amdatu.blueprint.demonstrator.todo.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.amdatu.blueprint.demonstrator.todo.api.Todo;
import org.amdatu.blueprint.demonstrator.todo.api.TodoService;
import org.amdatu.blueprint.demonstrator.todo.spi.TodoCollectionService;


public class TodoServiceImpl implements TodoService {

    private volatile TodoCollectionService m_todoCollectionService;


    @Override
    public List<Todo> list() {
        return m_todoCollectionService
                .all()
                .collect(Collectors.toList());
    }

    @Override
    public Todo add(String name) {
        Todo todo = new Todo(UUID.randomUUID().toString(), name, false);
        return m_todoCollectionService.add(todo);
    }

    @Override
    public void markDone(String id) {
        Todo todo = m_todoCollectionService
                .find(id)
                .orElseThrow(() -> new RuntimeException("Todo not found"));

        if (todo.isDone()) {
            throw new RuntimeException("Todo already done");
        }

        m_todoCollectionService.update(new Todo(todo.getId(), todo.getName(), true));
    }

    @Override
    public void markNotDone(String id) {
        Todo todo = m_todoCollectionService
                .find(id)
                .orElseThrow(() -> new RuntimeException("Todo not found"));

        if (!todo.isDone()) {
            throw new RuntimeException("Todo not done yet");
        }

        m_todoCollectionService.update(new Todo(todo.getId(), todo.getName(), false));
    }

    @Override
    public void delete(String id) {
        Todo todo = m_todoCollectionService
                .find(id)
                .orElseThrow(() -> new RuntimeException("Todo not found"));;

        if (!todo.isDone()) {
            throw new RuntimeException("Todo not done yet");
        }

        m_todoCollectionService.delete(todo.getId());
    }
}
