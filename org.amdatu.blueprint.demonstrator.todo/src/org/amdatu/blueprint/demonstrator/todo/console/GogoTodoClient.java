package org.amdatu.blueprint.demonstrator.todo.console;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.amdatu.blueprint.demonstrator.todo.api.Todo;
import org.amdatu.blueprint.demonstrator.todo.api.TodoService;
import org.apache.felix.dm.annotation.api.Property;

@Property(name = "osgi.command.scope", value = "todo")
@Property(name = "osgi.command.function", value = {"list", "add", "remove", "done", "testje"})
public class GogoTodoClient {

    private volatile TodoService m_todoService;

    private Map<Integer, String> numberToId = new HashMap<>();

    public void list() {
        List<Todo> todoList = m_todoService.list();
        for (int i = 0; i < todoList.size(); i++) {
            Todo todo = todoList.get(i);
            numberToId.put(i, todo.getId());
            System.out.printf("[%d]\t %s [%s]\n", i, todo.getName(), todo.isDone() ? "x" : " ");
        }
    }

    public void add(String name) {
        m_todoService.add(name);
        list();
    }

    public void remove(Integer number) {
        String todoId = numberToId.computeIfAbsent(number, (n) -> { throw new RuntimeException("Invalid todo number"); });
        m_todoService.delete(todoId);
    }

    public void done(Integer number) {
        String todoId = numberToId.computeIfAbsent(number, (n) -> { throw new RuntimeException("Invalid todo number"); });
        m_todoService.markDone(todoId);
    }

}
