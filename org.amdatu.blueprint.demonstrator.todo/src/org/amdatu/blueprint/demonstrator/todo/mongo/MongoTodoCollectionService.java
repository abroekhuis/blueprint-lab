package org.amdatu.blueprint.demonstrator.todo.mongo;


import com.mongodb.WriteResult;
import org.amdatu.blueprint.demonstrator.todo.api.Todo;
import org.amdatu.blueprint.demonstrator.todo.spi.TodoCollectionService;
import org.amdatu.mongo.MongoDBService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.String.format;

public class MongoTodoCollectionService implements TodoCollectionService {

    private static final String TODO_COLLECTION = "todo";

    private volatile MongoDBService m_mongoDBService;
    private volatile MongoCollection m_todoCollection;

    @Start
    protected final void start() {
        @SuppressWarnings("deprecation")
        Jongo jongo = new Jongo(m_mongoDBService.getDB());
        m_todoCollection = jongo.getCollection(TODO_COLLECTION);
    }

    @Override
    public Todo add(Todo todo) {
        m_todoCollection.save(MongoTodo.fromTodo(todo));
        return find(todo.getId())
                .orElseThrow(() -> new IllegalArgumentException("Something bad happened todo stored but could not be retrieved"));
    }

    @Override
    public void update(Todo todo) {
        WriteResult result = m_todoCollection.update(format("{id: '%s'}", todo.getId()))
                .with(MongoTodo.fromTodo(todo));

        if (result.getN() != 1) {
            throw new IllegalStateException(format("Update of todo with id '%s' failed", todo.getId()));
        }
    }

    @Override
    public void delete(String id) {
        WriteResult result = m_todoCollection.remove(format("{id: '%s'}", id));
        if (result.getN() != 1) {
            throw new IllegalStateException(format("Employee with id '%s' not found", id));
        }
    }

    @Override
    public Stream<Todo> all() {
        MongoCursor<MongoTodo> cursor = m_todoCollection.find().as(MongoTodo.class);
        return StreamSupport.stream(cursor.spliterator(), false)
                .map(MongoTodo::toTodo);
    }

    @Override
    public Optional<Todo> find(String id) {
        MongoCursor<MongoTodo> cursor = m_todoCollection
                .find(format("{id: '%s'}", id))
                .as(MongoTodo.class);

        return StreamSupport.stream(cursor.spliterator(), false)
                .findFirst()
                .map(MongoTodo::toTodo);
    }
}
