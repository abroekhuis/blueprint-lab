package org.amdatu.blueprint.demonstrator.todo.mongo;


import org.amdatu.blueprint.demonstrator.todo.api.Todo;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

public class MongoTodo {

    @MongoId
    @MongoObjectId
    private String mongoId;

    private String id;

    private String name;

    private boolean done;

    public static MongoTodo fromTodo(Todo todo) {
        MongoTodo mongoTodo = new MongoTodo();
        mongoTodo.setId(todo.getId());
        updateMongoTodo(todo, mongoTodo);
        return mongoTodo;
    }

    public static void updateMongoTodo(Todo source, MongoTodo target) {
        target.setName(source.getName());
        target.setDone(source.isDone());
    }

    public static Todo toTodo(MongoTodo mongoTodo) {
        return new Todo(mongoTodo.getId(), mongoTodo.getName(), mongoTodo.isDone());
    }

    public String getMongoId() {
        return mongoId;
    }

    public void setMongoId(String mongoId) {
        this.mongoId = mongoId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
