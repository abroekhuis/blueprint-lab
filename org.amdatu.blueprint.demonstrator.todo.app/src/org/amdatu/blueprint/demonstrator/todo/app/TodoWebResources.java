package org.amdatu.blueprint.demonstrator.todo.app;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PATTERN;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PREFIX;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Component( provides = Object.class, properties = {
    @Property(name = HTTP_WHITEBOARD_RESOURCE_PATTERN, value = "/todo/*"),
    @Property(name = HTTP_WHITEBOARD_RESOURCE_PREFIX, value = "/static/todo")
})
public class TodoWebResources {

}
