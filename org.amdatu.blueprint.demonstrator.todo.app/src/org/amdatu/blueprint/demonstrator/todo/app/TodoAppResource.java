package org.amdatu.blueprint.demonstrator.todo.app;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amdatu.blueprint.demonstrator.todo.api.Todo;
import org.amdatu.blueprint.demonstrator.todo.api.TodoService;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;

// TODO: Use rest application instead of direct resource registration


@Component(provides = Object.class)
@Property(name = AmdatuWebRestConstants.JAX_RS_RESOURCE_NAME, value = "TodoResource")
@Property(name = AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE, value = "rest")
@Path("todo")
@Produces(MediaType.APPLICATION_JSON)
public class TodoAppResource {

    @ServiceDependency
    public TodoService m_todoService;

    @GET
    public List<Todo> list() {
        return m_todoService.list();
    }

    @GET
    @Path("{id}")
    public Todo get(@PathParam("id") String id) {
        return m_todoService.list().stream().filter(todo -> todo.getId().equals(id)).findAny()
                .orElseThrow(() -> new WebApplicationException(Response.Status.NOT_FOUND));
    }

    static class TodoStatus {
        public Boolean done;
    }

    @PUT
    @Path("{id}")
    public void setDone(@PathParam("id") String id, TodoStatus todoStatus) {
        if (todoStatus.done) {
            m_todoService.markDone(id);
        } else {
            m_todoService.markNotDone(id);
        }
    }

    @POST
    public Todo add(String name) {
       return m_todoService.add(name);
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") String id) {
        m_todoService.delete(id);
    }

}
