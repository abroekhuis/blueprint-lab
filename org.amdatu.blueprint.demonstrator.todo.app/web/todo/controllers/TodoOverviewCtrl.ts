import TodoService = require('services/TodoService');

class TodoOverviewCtrl  {

    static $inject = ['todoService', 'todos'];
    static resolve = {
        todos: ['todoService', (todoService) => {
            return todoService.getTodos();
        }]
    };

    constructor(public todoService:TodoService, public todos:any[]) {
    }

    save(todo: any): void {
        this.todoService.updateStatus(todo.id, todo.done);
    }

    add(name: string): void {
        this.todoService.add(name).then(todo => {
            this.todos.push(todo);
        })
    }

    delete(todo: any): void {
        this.todoService.delete(todo.id)
            .then(() => {
                for(var i in this.todos){
                    if(this.todos[i].id == todo.id){
                        this.todos.splice(Number(i),1);
                        break;
                    }
                }
            })
            .catch(() => {
                alert('Failed to remove todo: ' + todo.name);
            });
    }

}

export = TodoOverviewCtrl;
