class TodoService {

    static $inject = ['$http'];

    private todoServiceUrl = "/rest/todo/";

    constructor(public $http: angular.IHttpService) {
    }

    getTodos(): angular.IPromise<any> {
        return this.$http.get(this.todoServiceUrl).then(r => r.data);
    }

    updateStatus(id: string, done: boolean): angular.IPromise<any> {
        return this.$http.put(this.todoServiceUrl + id , {done: done}).then(r => r.data);
    }

    add(name: string): angular.IPromise<any> {
        return this.$http.post(this.todoServiceUrl, name).then(r => r.data);
    }

    delete(id: string): angular.IPromise<any> {
        return this.$http.delete(this.todoServiceUrl + id).then(r => r.data);
    }
}

export = TodoService
