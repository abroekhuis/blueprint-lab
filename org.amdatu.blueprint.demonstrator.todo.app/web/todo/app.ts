/// <amd-dependency path="angular"/>
/// <amd-dependency path="angular-ui-router"/>
/// <amd-dependency path="org.amdatu.blueprint.demonstrator.ui.shared"/>
/// <amd-dependency path="./todoTemplates"/>

'use strict'
import angular = require('angular');
import TodoService = require('./services/TodoService');
import TodoOverviewCtrl = require('./controllers/TodoOverviewCtrl');

var modulenName = 'todo';
var partialsDir = modulenName + '/partials/';

// TODO: Webstorm keeps whining about the angular.IModule  ... :(
// var ngModule: angular.IModule = angular.module(modulenName, ['ui.router','todoTemplates']);
var ngModule = angular.module(modulenName, ['ui.router','todoTemplates']);
ngModule.config(['$urlRouterProvider','$stateProvider', function($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/todo');

    $stateProvider
        .state("todo", {
            url: "/todo",
            controller: 'TodoOverviewCtrl as overviewCtrl',
            resolve: TodoOverviewCtrl.resolve,
            templateUrl: partialsDir + 'overview.html'
        })
}]);

ngModule.service('todoService', TodoService);
ngModule.controller('TodoOverviewCtrl', TodoOverviewCtrl);

angular.bootstrap(document, [modulenName, 'org.amdatu.blueprint.demonstrator.ui.shared']);
