define(["require", "exports"], function (require, exports) {
    "use strict";
    var TodoOverviewCtrl = (function () {
        function TodoOverviewCtrl(todoService, todos) {
            this.todoService = todoService;
            this.todos = todos;
        }
        TodoOverviewCtrl.prototype.save = function (todo) {
            this.todoService.updateStatus(todo.id, todo.done);
        };
        TodoOverviewCtrl.prototype.add = function (name) {
            var _this = this;
            this.todoService.add(name).then(function (todo) {
                _this.todos.push(todo);
            });
        };
        TodoOverviewCtrl.prototype.delete = function (todo) {
            var _this = this;
            this.todoService.delete(todo.id)
                .then(function () {
                for (var i in _this.todos) {
                    if (_this.todos[i].id == todo.id) {
                        _this.todos.splice(Number(i), 1);
                        break;
                    }
                }
            })
                .catch(function () {
                alert('Failed to remove todo: ' + todo.name);
            });
        };
        return TodoOverviewCtrl;
    }());
    TodoOverviewCtrl.$inject = ['todoService', 'todos'];
    TodoOverviewCtrl.resolve = {
        todos: ['todoService', function (todoService) {
                return todoService.getTodos();
            }]
    };
    return TodoOverviewCtrl;
});
