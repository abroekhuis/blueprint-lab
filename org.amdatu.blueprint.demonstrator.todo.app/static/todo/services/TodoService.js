define(["require", "exports"], function (require, exports) {
    "use strict";
    var TodoService = (function () {
        function TodoService($http) {
            this.$http = $http;
            this.todoServiceUrl = "/rest/todo/";
        }
        TodoService.prototype.getTodos = function () {
            return this.$http.get(this.todoServiceUrl).then(function (r) { return r.data; });
        };
        TodoService.prototype.updateStatus = function (id, done) {
            return this.$http.put(this.todoServiceUrl + id, { done: done }).then(function (r) { return r.data; });
        };
        TodoService.prototype.add = function (name) {
            return this.$http.post(this.todoServiceUrl, name).then(function (r) { return r.data; });
        };
        TodoService.prototype.delete = function (id) {
            return this.$http.delete(this.todoServiceUrl + id).then(function (r) { return r.data; });
        };
        return TodoService;
    }());
    TodoService.$inject = ['$http'];
    return TodoService;
});
