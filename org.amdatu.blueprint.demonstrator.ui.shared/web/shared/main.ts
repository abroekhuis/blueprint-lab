// The following path mappings are used in the TypeScript code by ///<amd-dependency> directives
require.config({
    "baseUrl": "../",
    "paths": {
        "angular": "lib/angular/angular",
        "angular-sanitize": "lib/angular-sanitize/angular-sanitize",
        "angular-ui-router": "lib/angular-ui-router/release/angular-ui-router",
        "angular-dynamic-locale": "lib/angular-dynamic-locale/src/tmhDynamicLocale",
        "angular-translate": "lib/angular-translate/angular-translate",
        "angular-translate-loader-partial": "lib/angular-translate-loader-partial/angular-translate-loader-partial",
        "angular-css-injector": "lib/angular-css-injector/angular-css-injector",
        "requirejs-dplugins": "lib/requirejs-dplugins",
        "requirejs-text": "lib/requirejs-text",
        "jquery": "lib/jquery/dist/jquery",
        "async": "lib/requirejs-plugins/src/async",
        "lodash": "lib/lodash/lodash",
        "org.amdatu.blueprint.demonstrator.ui.shared": "shared/app"
    },
    "shim": {
        "angular": {
            "exports": "angular",
            "deps": ["jquery"]
        },
        "angular-sanitize": {
            "deps": ["angular"]
        },
        "angular-ui-router": {
            "deps": ["angular"]
        },
        "angular-translate": {
            "deps": ["angular"]
        },
        "angular-translate-loader-partial": {
            "deps": ["angular", "angular-translate"]
        },
        "angular-css-injector": {
            "deps": ["angular"]
        },
        "angular-dynamic-locale": {
            "deps": ["angular"]
        },
        "lodash": {
            "exports": "_"
        }
    }

});
