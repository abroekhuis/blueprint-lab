/// <amd-dependency path="angular"/>
/// <amd-dependency path="angular-ui-router"/>
/// <amd-dependency path="angular-css-injector"/>
/// <amd-dependency path="shared/sharedTemplates"/>

'use strict'
import angular = require('angular');
import TopBarService = require('shared/topbar/TopBarService');
import TopBarDirective = require('shared/topbar/TopBarDirective');
import InjectCssDirective = require('shared/injectCss/InjectCssDirective');

var BASE_URL:string = '//localhost:8989';
var modulename:string = 'org.amdatu.blueprint.demonstrator.ui.shared';
var ngModule: angular.IModule = angular.module(modulename, ['angular.css.injector', 'sharedTemplates']);

ngModule.service('topBarService', TopBarService);
ngModule.directive('topbar', <any>TopBarDirective);
ngModule.directive('injectCss', <any>InjectCssDirective);
ngModule.constant('BASE_URL', BASE_URL)
