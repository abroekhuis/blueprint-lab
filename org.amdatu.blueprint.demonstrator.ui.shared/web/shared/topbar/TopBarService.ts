class TopBarService {

    static $inject = ['$http', 'BASE_URL', '$q'];

    topBarUrl:string;

    constructor(public $http, public BASE_URL, private $q) {
        this.topBarUrl = this.BASE_URL + '/apps';
    }

    getApps(): angular.IPromise<any[]> {

        var apps = [{
            name: "todo",
            path: "/todo"
        }, {
            name: "blub",
            path: "/blub"
        }];

        var d: angular.IDeferred<any[]> = this.$q.defer();
        var p: angular.IPromise<any[]> = d.promise;
        d.resolve(apps);

        return p;
    }

}

export = TopBarService
