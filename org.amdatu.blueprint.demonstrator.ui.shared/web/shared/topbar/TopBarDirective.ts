import TopBarService = require('./TopBarService');

class TopBarDirective {
    public static $inject = ['topBarService'];
    constructor(topBarService:TopBarService) {
        var directive: angular.IDirective = {};
        directive.restrict = 'E';
        directive.templateUrl = 'shared/topbar/topbar.html';
        directive.link = function($scope:any, element, attrs: any) {
            topBarService.getApps().then(apps => $scope.apps = apps);
        };
        return directive;
    }
}

export = TopBarDirective;
