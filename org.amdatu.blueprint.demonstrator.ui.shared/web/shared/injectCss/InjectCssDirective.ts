class InjectCssDirective {
    public static $inject = ['cssInjector'];
    constructor(cssInjector) {
        var directive: angular.IDirective = {};
        directive.restrict = "E";
        directive.link = (scope, element, attrs: any) => {
            cssInjector.add(attrs.path);
        };
        return directive;
    }
}

export = InjectCssDirective;
