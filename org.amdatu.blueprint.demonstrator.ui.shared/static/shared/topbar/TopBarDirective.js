define(["require", "exports"], function (require, exports) {
    "use strict";
    var TopBarDirective = (function () {
        function TopBarDirective(topBarService) {
            var directive = {};
            directive.restrict = 'E';
            directive.templateUrl = 'shared/topbar/topbar.html';
            directive.link = function ($scope, element, attrs) {
                topBarService.getApps().then(function (apps) { return $scope.apps = apps; });
            };
            return directive;
        }
        return TopBarDirective;
    }());
    TopBarDirective.$inject = ['topBarService'];
    return TopBarDirective;
});
