define(["require", "exports"], function (require, exports) {
    "use strict";
    var TopBarService = (function () {
        function TopBarService($http, BASE_URL, $q) {
            this.$http = $http;
            this.BASE_URL = BASE_URL;
            this.$q = $q;
            this.topBarUrl = this.BASE_URL + '/apps';
        }
        TopBarService.prototype.getApps = function () {
            var apps = [{
                    name: "todo",
                    path: "/todo"
                }, {
                    name: "blub",
                    path: "/blub"
                }];
            var d = this.$q.defer();
            var p = d.promise;
            d.resolve(apps);
            return p;
        };
        return TopBarService;
    }());
    TopBarService.$inject = ['$http', 'BASE_URL', '$q'];
    return TopBarService;
});
