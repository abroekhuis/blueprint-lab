/// <amd-dependency path="angular"/>
/// <amd-dependency path="angular-ui-router"/>
/// <amd-dependency path="angular-css-injector"/>
/// <amd-dependency path="shared/sharedTemplates"/>
define(["require", "exports", "angular", "shared/topbar/TopBarService", "shared/topbar/TopBarDirective", "shared/injectCss/InjectCssDirective", "angular", "angular-ui-router", "angular-css-injector", "shared/sharedTemplates"], function (require, exports, angular, TopBarService, TopBarDirective, InjectCssDirective) {
    'use strict';
    Object.defineProperty(exports, "__esModule", { value: true });
    var BASE_URL = '//localhost:8989';
    var modulename = 'org.amdatu.blueprint.demonstrator.ui.shared';
    var ngModule = angular.module(modulename, ['angular.css.injector', 'sharedTemplates']);
    ngModule.service('topBarService', TopBarService);
    ngModule.directive('topbar', TopBarDirective);
    ngModule.directive('injectCss', InjectCssDirective);
    ngModule.constant('BASE_URL', BASE_URL);
});
