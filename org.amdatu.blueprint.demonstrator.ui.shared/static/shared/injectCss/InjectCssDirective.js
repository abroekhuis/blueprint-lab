define(["require", "exports"], function (require, exports) {
    "use strict";
    var InjectCssDirective = (function () {
        function InjectCssDirective(cssInjector) {
            var directive = {};
            directive.restrict = "E";
            directive.link = function (scope, element, attrs) {
                cssInjector.add(attrs.path);
            };
            return directive;
        }
        return InjectCssDirective;
    }());
    InjectCssDirective.$inject = ['cssInjector'];
    return InjectCssDirective;
});
