var gulp = require('gulp'),
    rename = require('gulp-rename');

gulp.task('copyMain', function(done) {
    var streams = [];
    getAppConfigs().forEach(function(appConfig) {
        if (appConfig.hasOwnProperty("useExternalMainFrom")) {
            var sourceAppConfig = getAppConfig(appConfig.useExternalMainFrom);
            streams.push(gulp
                .src(sourceAppConfig.bundle + sourceAppConfig.output + '/*/main.js')
                .pipe(rename({dirname: ''}))
                .pipe(gulp.dest(getOutputFolder(appConfig))));
        }
    });

    mergeStreams(streams, done);
});
