var gulp = require('gulp'),
    fs = require('fs'),
    replace = require('gulp-replace'),
    rename = require('gulp-rename'),
    pedding = require('pedding'),
    jasminePhantomJs = require('gulp-jasmine2-phantomjs');

gulp.task('unitTests', function(done) {
    var streams = [],
        appsWithTests = getAppConfigs().filter(function(appConfig) {
            return appConfig.typescript ? appConfig.typescript.unitTests : false;
        });

    if (appsWithTests.length > 0) {
        done = pedding(appsWithTests.length, done);

        appsWithTests.forEach(function(appConfig) {
            // read main.js
            var rjsConfig = getFile(getOutputFolder(appConfig) + '/main.js');

            // adjust main.js
            if (appConfig.hasOwnProperty("useExternalLibsFrom")) {
                var sourceAppConfig = getAppConfig(appConfig.useExternalLibsFrom);
                Object.keys(rjsConfig.paths).forEach(function(modname) {
                     if (rjsConfig.paths[modname].indexOf('lib/') > -1) {
                         rjsConfig.paths[modname] = '../../' + sourceAppConfig.name + sourceAppConfig.output + '/' + rjsConfig.paths[modname];
                     }
                });
            }
            rjsConfig.baseUrl = './../../' + appConfig.name + appConfig.output;
            rjsConfig.paths['angular-mocks'] = '../../org.amdatu.ui.build/web-test/libs/angular-mocks';
            rjsConfig.shim['angular-mocks'] = { deps: [ 'angular' ] };

            // generate test html
            streams.push(
                gulp.src('web-test/test.html')
                    .pipe(replace('<!--APP_NAME-->', appConfig.name))
                    .pipe(replace('<!--REQUIREJS_CONFIG-->', JSON.stringify(rjsConfig)))
                    .pipe(replace('<!--UNITTESTS-->', appConfig.typescript.unitTests.map(function(testPath) {
                        return '"' + appConfig.folder + '/' + testPath + '"';
                    }).join(',')))
                    .pipe(rename(function(path) {
                        path.basename = path.basename + '-' + appConfig.name;
                    }))
                    .pipe(gulp.dest('web-test'))
                    .on('end', function() {
                        gulp.src('web-test/test-' + appConfig.name + '.html')
                            .pipe(jasminePhantomJs())
                            .on('end', done)
                            .on('error', done)
                    })
            );
        });
    } else {
        done();
    }
});
