var gulp = require('gulp'),
    fs = require('fs'),
    tap = require('gulp-tap'),
    http = require('http'),
    proxy = require('http-proxy-middleware'),
    webserver = require('gulp-webserver'),
    currentWebserver;

gulp.task('serve', function() {
    if (currentWebserver) {
        currentWebserver.emit('kill');
    }

    // default configuration
    var uiRunConf,
        runConf = {
            port: '8000',
            proxy
        };

    // try to load run configuration which can override the default configuration
    return gulp.src(['./../*/*.uirun','!./../*/*.uirun/**'])
        .pipe(tap(function(file) {
            uiRunConf = getFile('../' + file.relative);
            if (uiRunConf.port) runConf.port = uiRunConf.port;
            if (uiRunConf.proxy) runConf.proxy = proxy(uiRunConf.proxy.paths, { target: uiRunConf.proxy.target, logLevel: 'warn' });
        }))
        .on('end', function() {
            currentWebserver = gulp.src(getApps().map(function(app) {
                return '../' + app + (uiRunConf.pathToServe ? uiRunConf.pathToServe : APPCONFIGS[app].output);
            }).concat(['./web'])).pipe(webserver({
                port: runConf.port,
                livereload: true,
                directoryListing: false,
                middleware: [runConf.proxy]
            }));
            return currentWebserver;
        });

});
