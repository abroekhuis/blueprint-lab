var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer');

gulp.task('compileSass', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    return gulp
        .src(gulpif(WATCH_MODE, getAllSourcesFor('sass'), appConfig.bundle + '/' + appConfig.sass.sources))
        .pipe(sass({
	        sourceComments: 'map',
	        errLogToConsole: false,
	        onError: function (err) {
	            return notify().write(err);
	        }
    	}))
        .pipe(prefix("last 2 versions"))
        .pipe(gulpif(WATCH_MODE, gulp.dest(getOutputForFile), gulp.dest(appConfig.bundle + appConfig.output)));
});
