var gulp = require('gulp'),
    fs = require('fs'),
    revReplace = require('gulp-rev-replace');

gulp.task('replaceRev', function(done) {
    var streams = [];

    // copy the libs to bundles which are depending on them
    getAppConfigs().forEach(function(appConfig) {
        if (appConfig.typescript && appConfig.typescript.sources && appConfig.html && appConfig.html.sources) {
            var output = getOutputFolder(appConfig);
            var manifest = gulp.src(output + "/rev-manifest.json");
            if (fs.existsSync(output + "/index.html")) {
                streams.push(gulp.src([output + "/index.html", output + "/main*.js"])
                    .pipe(revReplace({manifest: manifest}))
                    .pipe(gulp.dest(output)));
            }
        }
    });

    mergeStreams(streams, done);
});
