var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    cache = require('gulp-cached');

gulp.task('copyJavascript', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    return gulp
        .src(gulpif(WATCH_MODE, getAllSourcesFor('javascript'), appConfig.bundle + '/' + appConfig.javascript.sources))
        .pipe(cache('javascript'))
        .pipe(gulpif(WATCH_MODE, gulp.dest(getOutputForFile), gulp.dest(appConfig.bundle + appConfig.output)));
});
