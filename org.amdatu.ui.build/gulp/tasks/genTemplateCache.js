var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    rename = require('gulp-rename'),
    templateCache = require('gulp-angular-templatecache');

gulp.task('genTemplateCache', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig),
        streams = [];

    var createCacheStream = function(appConfig) {
        if (appConfig.html) {
            var moduleName = appConfig.folder + 'Templates',
                outputFileName = moduleName + '.js';

            streams.push(gulp
                .src(appConfig.bundle + '/' + appConfig.html.sources)
                .pipe(templateCache(outputFileName, {
                    root: '',
                    standalone: true,
                    moduleSystem: 'RequireJS',
                    module: moduleName
                }))
                .pipe(rename(function(path) {
                    path.basename = appConfig.folder + '/' + path.basename; // prepend filename with app folder name
                }))
                .pipe(gulp.dest(appConfig.bundle + appConfig.output))
            );
        }
    }

    if (WATCH_MODE) {
        getAppConfigs().forEach(function(currentAppConfig) {
            createCacheStream(currentAppConfig);
        });
    } else {
        createCacheStream(appConfig);
    }

    return (streams.length > 0) ? mergeStreams(streams, done) : done();
});
