var gulp = require('gulp'),
    shell = require('gulp-shell'),
    styleguide = 'node node_modules/nucleus-styleguide/bin --verbose ';

gulp.task('genStyleguide', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);

    var generateStyleguide = function(appConfig) {
        if (appConfig.hasOwnProperty("styleguide")) {
            var files = prefixSources(appConfig.styleguide.sources, appConfig.bundle + '/').join(" ");
            var config = '--files ' + files + ' ';
            config += '--target ' + appConfig.bundle + appConfig.output + '/' + appConfig.styleguide.output + ' ';
            config += '--css ' + appConfig.styleguide.mainCss + ' ';
            config += '--title "' + appConfig.styleguide.title + '"';
            shell.task([styleguide + config])();
        }
    }

    if (WATCH_MODE) {
        getAppConfigs().forEach(function(currentAppConfig) {
            generateStyleguide(currentAppConfig);
        });
    } else {
        generateStyleguide(appConfig);
    }

    return done();
});
