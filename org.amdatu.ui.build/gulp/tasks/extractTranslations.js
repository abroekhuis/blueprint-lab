var gulp = require('gulp'),
    change = require('gulp-change'),
    gettext = require('gulp-angular-gettext');

gulp.task('extractTranslations', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig),
        streams = [],
        gettextOptions = {
            markerName: 'translate'
        };

    var createCacheStream = function(appConfig) {
        if (!appConfig.extractTranslationsToPot) return;

        if (appConfig.extractTranslationsToPot.gettextOptions) {
            Object.assign(gettextOptions, appConfig.extractTranslationsToPot.gettextOptions);
        }

        streams.push(gulp
            .src(prefixSources(appConfig.extractTranslationsToPot.sources, appConfig.bundle + '/'))
            .pipe(change(function(content) {
                //strip out amd dependency and reference tags to prevent gettext seeing those as documentation
                return content.replace(/\/{3} <(amd|reference).*/g, '');
            }))
            .pipe(gettext.extract(appConfig.extractTranslationsToPot.outputFilename, gettextOptions))
            .pipe(gulp.dest(appConfig.bundle + appConfig.extractTranslationsToPot.outputDir)));
    }

    if (WATCH_MODE) {
        getAppConfigs().forEach(function(currentAppConfig) {
            createCacheStream(currentAppConfig);
        });
    } else {
        createCacheStream(appConfig);
    }

    return (streams.length > 0) ? mergeStreams(streams, done) : done();
});
