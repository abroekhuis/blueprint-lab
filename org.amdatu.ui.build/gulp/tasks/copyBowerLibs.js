var gulp = require('gulp'),
    mainBowerFiles = require('main-bower-files');

gulp.task('copyBowerLibs', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    if (!appConfig.bower_components) return done();
    var libDir = appConfig.libsDirectory ? appConfig.libsDirectory : 'lib';
    return gulp.src(mainBowerFiles({ paths: appConfig.bundle }), { base: appConfig.bundle + '/bower_components' })
        .pipe(gulp.dest(appConfig.bundle + appConfig.output + '/' + libDir));
});
