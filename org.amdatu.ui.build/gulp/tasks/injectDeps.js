var gulp = require('gulp'),
    fs = require('fs'),
    replace = require('gulp-replace');

gulp.task('injectDeps', function(done) {
    var streams = [];

    getAppConfigs().forEach(function(appConfig) {
        var output = getOutputFolder(appConfig);

        if (appConfig.distDependencies && fs.existsSync(output + "/index.html")) {
            var depUrls = '';
            appConfig.distDependencies.forEach(function(depAppName) {
                var depApp = getAppConfig(depAppName);
                var revManifest = JSON.parse(fs.readFileSync(getOutputFolder(depApp) + "/rev-manifest.json"));
                depUrls += ('<script src="../' + depApp.folder + '/' + revManifest['main.js'] + '"></script>');

                streams.push(gulp.src(output + "/index.html")
                    .pipe(replace())
                    .pipe(replace('require.js"></script>', 'require.js"></script>' + depUrls))
                    .pipe(gulp.dest(output))
                );
            });
        }
    });

    mergeStreams(streams, done);
});
