var gulp = require('gulp'),
    fs = require('fs'),
    gulpif = require('gulp-if'),
    replace = require('gulp-replace');

gulp.task('replaceConstants', function(done) {
    var streams = [];

    getAppConfigs().forEach(function(appConfig) {
        if (appConfig.replaceConstants) {
            var output = getOutputFolder(appConfig);
            var manifest = JSON.parse(fs.readFileSync(output + '/rev-manifest.json', "utf8"));
            var stream = gulp.src([output + '/' + appConfig.appFileName + '.js',output + '/' + manifest["main.js"]]);
            Object.keys(appConfig.replaceConstants).forEach(function(key) {
                stream.pipe(replace(key, appConfig.replaceConstants[key]))
            });
            stream.pipe(gulp.dest(output));
            streams.push(stream);
        }
    });

    mergeStreams(streams, done);
});
