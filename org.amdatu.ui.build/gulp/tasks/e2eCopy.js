var gulp = require('gulp'),
    gutil = require('gulp-util'),
    tap = require("gulp-tap");

gulp.task('e2eCopy', function(done) {
    return gulp
        .src(prefixSources(E2E_CONFIG.assets, E2E_BUNDLE + '/'))
        .pipe(gulp.dest(E2E_BUNDLE + '/static'));
});
