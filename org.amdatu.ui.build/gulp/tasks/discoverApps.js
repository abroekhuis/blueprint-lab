var gulp = require('gulp'),
    fs = require('fs'),
    gutil = require('gulp-util'),
    requireUncached = require('require-uncached'),
    jsoncombine = require("gulp-jsoncombine"),
    clipEmptyFiles = require('gulp-clip-empty-files'),
    del = require('del'),
    replace = require('gulp-replace'),
    rename = require('gulp-rename');

gulp.task('discoverApps', function(done) {
    del.sync(['gulp/appconfigs.js'], {force: true});
    return gulp
        .src(['./../*/*.ui','!./../*/*.ui/**'])
        .pipe(clipEmptyFiles())
        .pipe(jsoncombine("appconfigs.js", {
            dataConverter: function(data) {
                return new Buffer('module.exports = ' + JSON.stringify(data));
            },
            pathConverter: function(file) {
                return extractAppFolderFromPath(file.relative);
            }
        }))
        .pipe(gulp.dest('gulp'))
        .on('end', function() {
            if (fs.existsSync('./gulp/appconfigs.js')) {
                APPCONFIGS = requireUncached('../appconfigs');
                Object.keys(APPCONFIGS).forEach(function(appName) {
                    validateAppConfiguration(appName);
                    APPCONFIGS[appName].name = appName;
                    APPCONFIGS[appName].bundle = './../' + appName;

                    registerAppTasks(appName, false);
                });

                gulp.src('web/modui-template.html')
                    .pipe(replace('<!--APPS-->', Object.keys(APPCONFIGS).map(function(appName) {
                        return '<a href="/'+APPCONFIGS[appName].folder+'">'+appName+'</a>';
                    }).join('<br>')))
                    .pipe(rename(function(path) {
                        path.basename = 'modui'
                    }))
                    .pipe(gulp.dest('web'))
            } else {
                done();
                gutil.log(gutil.colors.red('No app configurations found! Exiting Modular UI...'));
                process.exit(0);
            }

            done();
        });
});
