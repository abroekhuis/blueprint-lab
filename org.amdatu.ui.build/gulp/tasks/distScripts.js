var gulp = require('gulp'),
    rjs = require('gulp-requirejs'),
    ngAnnotate = require('gulp-ng-annotate'),
    rev = require('gulp-rev'),
    uglify = require('gulp-uglify'),
    fs = require('fs');

gulp.task('distScripts', function(done) {
    var streams = [];

    getAppConfigs().forEach(function(appConfig) {
        var libDir = appConfig.libsDirectory ? appConfig.libsDirectory : 'lib';
        if (appConfig.bower_components) {
            streams.push(
                gulp.src(appConfig.bundle + appConfig.output + '/' + libDir + '/requirejs/require.js')
                    .pipe(uglify({
                        mangle: false // Since ng-min doesn't seem to catch all minification problems, don't mangle identifiers
                    }))
                    .pipe(gulp.dest(appConfig.bundle + appConfig.output + '/' + libDir + '/requirejs/'))
            );
        }

        if (appConfig.typescript && appConfig.typescript.sources) {
            var baseUrl = appConfig.bundle + appConfig.output;
            var output = baseUrl + '/' + appConfig.folder;
            var mainFileName = 'main.js';

            // get main.js, adjust it (exclude all external dependencies as we want to distribute each app individually) and use that as config for rjs optimizer
            var rjsConfig = getFile(output + '/main.js');
            Object.keys(rjsConfig.paths).forEach(function(modname) {
                if (!appConfig.bower_components || rjsConfig.paths[modname].indexOf(libDir + '/') !== 0) {
                    rjsConfig.paths[modname] = 'empty:';
                }
            });

            // push app dependencies as TS compiler option paths, to be able to resolve modules of other apps
            if (appConfig.distDependencies) {
                appConfig.distDependencies.forEach(function(dependency) {
                    rjsConfig.paths[getAppConfig(dependency).folder] = 'empty:';
                });
            }

            streams.push(rjs({
                    baseUrl: baseUrl,
                    name: appConfig.folder + '/main',
                    out: mainFileName,
                    include: [appConfig.folder + '/' + appConfig.appFileName],
                    paths: rjsConfig.paths,
                    shim: rjsConfig.shim,
                })
                .pipe(ngAnnotate())
                .pipe(uglify({
                    mangle: false // Since ng-min doesn't seem to catch all minification problems, don't mangle identifiers
                }))
                .pipe(rev())
                .pipe(gulp.dest(output))
                .pipe(rev.manifest({base: output, path: output + "/rev-manifest.json", merge: true}))
                .pipe(gulp.dest(output))
            );
        }
    });

    mergeStreams(streams, done);
});
