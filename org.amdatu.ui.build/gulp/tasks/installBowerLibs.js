var gulp = require('gulp'),
    bower = require('gulp-bower');

gulp.task('installBowerLibs', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    if (!appConfig.bower_components || appConfig.bower_components === 'copy') return done();
    return bower({ cwd: appConfig.bundle })
        .on('end', done);
});
