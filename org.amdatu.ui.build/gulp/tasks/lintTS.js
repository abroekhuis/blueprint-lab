var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    tslint = require('gulp-tslint');

gulp.task('lintTS', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    return gulp
        .src(gulpif(WATCH_MODE, getAllSourcesFor('typescript'), appConfig.bundle + '/' + appConfig.typescript.sources))
        .pipe(tslint({
            formatter: "verbose",
            configuration: "./tslint.json"
        }))
        .pipe(tslint.report({
            emitError: false,
            summarizeFailureOutput: true
        }));
});
