var gulp = require('gulp'),
    mainBowerFiles = require('main-bower-files');;

gulp.task('copyLibs', function(done) {
    var streams = [];

    // copy the libs to bundles which are depending on them
    getAppConfigs().forEach(function(appConfig) {
        if (appConfig.hasOwnProperty("useExternalLibsFrom")) {
            var sourceAppConfig = getAppConfig(appConfig.useExternalLibsFrom);
            var libDir = sourceAppConfig.libsDirectory ? sourceAppConfig.libsDirectory : 'lib';
            streams.push(gulp
                .src(mainBowerFiles({ paths: sourceAppConfig.bundle }), { base: sourceAppConfig.bundle + '/bower_components' })
                .pipe(gulp.dest(appConfig.bundle + appConfig.output + '/' + libDir)));
        }
    });

    mergeStreams(streams, done);
});
