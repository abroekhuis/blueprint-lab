var gulp = require('gulp'),
    del = require('del');

gulp.task('clean', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    del.sync([
        '!./' + appConfig.bundle + appConfig.output + '/.gitignore',
        appConfig.bundle + appConfig.output + "/*",
        'web-test/test-*.html'
    ], {force: true});
    done();
});
