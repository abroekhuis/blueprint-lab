var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    cache = require('gulp-cached');

gulp.task('copyHtml', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    return gulp
        .src(gulpif(WATCH_MODE, getAllSourcesFor('html'), appConfig.bundle + '/' + appConfig.html.sources))
        .pipe(cache('html'))
        .pipe(gulpif(WATCH_MODE, gulp.dest(getOutputForFile), gulp.dest(appConfig.bundle + appConfig.output)));
});
