var gulp = require('gulp'),
    del = require('del');

gulp.task('cleanDist', function(done) {
    var globArr = [];

    getAppConfigs().forEach(function(appConfig) {
        var output = getOutputFolder(appConfig);
        globArr.push(output + '/**/*.ts');
        if (!appConfig.javascript) globArr.push(output + '/**/*.js');
        globArr.push(output + '/**/rev-manifest.json');
        if (appConfig.html && appConfig.html.sources) globArr.push(output + appConfig.html.sources);
        globArr.push('!' + output + '/main-*.js');
        globArr.push('!' + output + '/index.html');
        globArr.push('!' + output + '/' + appConfig.appFileName + '.js');
        globArr.push('!' + output + '/lib/requirejs/*');
        globArr.push('!' + output + '/lib/requirejs-*/**/*');
        globArr.push('!' + output + '/lib/angular-i18n/**/*');
    });
    del.sync(globArr, {force: true});
    done();
});
