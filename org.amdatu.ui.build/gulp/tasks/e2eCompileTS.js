var gulp = require('gulp'),
    typescript = require('gulp-typescript'),
    tsProject = typescript.createProject({
        typescript: require('typescript'),
        declaration: false,
        declarationFiles: false,
        noResolve: false,
        target: 'ES5',
        module: 'commonjs',
        types: [''] //don't include the node_modules/@types
    });

gulp.task('e2eCompileTS', function(done) {
    var sources = E2E_CONFIG.sources.concat(E2E_CONFIG.definitions);
    var tsResult = gulp.src(prefixSources(sources, E2E_BUNDLE + '/'))
        .pipe(tsProject());
    return tsResult.js
        .pipe(gulp.dest(E2E_BUNDLE + '/static'));
});
