var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    cache = require('gulp-cached');

gulp.task('copyFonts', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    return gulp
        .src(gulpif(WATCH_MODE, getAllSourcesFor('fonts'), appConfig.bundle + '/' + appConfig.fonts.sources))
        .pipe(cache('fonts'))


        
        .pipe(gulpif(WATCH_MODE, gulp.dest(getOutputForFile), gulp.dest(appConfig.bundle + appConfig.output)));
});
