var gulp = require('gulp'),
    gutil = require('gulp-util'),
    gulpif = require('gulp-if'),
    tap = require('gulp-tap'),
    rename = require('gulp-rename'),
    templateCache = require('gulp-angular-templatecache');

gulp.task('genTranslationCache', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig),
        streams = [];

    var createCacheStream = function(appConfig) {
        if (appConfig.json) {
            var moduleName = appConfig.folder + 'Translations',
                outputFileName = moduleName + '.js',
                filename;

            streams.push(gulp
                .src(appConfig.bundle + '/' + appConfig.json.sources)
                .pipe(tap(function(file, t) {
                    filename = file.relative;
                }))
                .pipe(templateCache(outputFileName, {
                    root: '/',
                    standalone: true,
                    moduleSystem: 'RequireJS',
                    module: moduleName,
                    templateHeader: 'angular.module("<%= module %>"<%= standalone %>).run(["$translationCache", function($translationCache) {',
                    templateBody: '$translationCache.put("<%= url %>",<%= contents %>);'
                }))
                .on('error', function(error) { // on error don't crash gulp but log the error and continue
                    gutil.log(gutil.colors.red('Error in JSON file ' + gutil.colors.yellow(filename) + ': ' + error.toString()));
                    this.emit('end');
                })
                .pipe(rename(function(path) {
                    path.basename = appConfig.folder + '/' + path.basename; // prepend filename with app folder name
                }))
                .pipe(gulp.dest(appConfig.bundle + appConfig.output))
            );
        }
    }

    if (WATCH_MODE) {
        getAppConfigs().forEach(function(currentAppConfig) {
            createCacheStream(currentAppConfig);
        });
    } else {
        createCacheStream(appConfig);
    }

    return (streams.length > 0) ? mergeStreams(streams, done) : done();
});
