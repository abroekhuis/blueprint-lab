var gulp = require('gulp'),
    del = require('del');

gulp.task('e2eClean', function(done) {
    del.sync([E2E_BUNDLE + '/static/**/*'], {force: true});
    done();
});
