var gulp = require('gulp'),
    path = require('path'),
    tap = require('gulp-tap'),
    gulpif = require('gulp-if'),
    cache = require('gulp-cached'),
    series = require('stream-series'),
    typescript = require('gulp-typescript');

gulp.task('compileTS', function(done, appConfig) {
    var unique = function(arr) {
        return arr.filter(function(el, pos) {
            return arr.indexOf(el) === pos;
        })
    };

    var compileTypescript = function(appConfig, returnResult) {
        var streams = [];

        // add TS app files
        streams.push(gulp
            .src(appConfig.bundle + '/' + appConfig.typescript.sources)
            .pipe(cache('typescript'))
        );

        // add TS definition files
        if (appConfig.typescript.definitions) {
            var tsDefs = appConfig.typescript.definitions;
            tsDefs.forEach(function(tsDefLocation) {
                tsDefLocation = (tsDefLocation.startsWith('..') ? './' : appConfig.bundle + '/') + tsDefLocation;
                streams.push(gulp.src(tsDefLocation));
            });
        }

        // add TS unittest definition files
        if (appConfig.typescript.unitTests) {
            streams.push(gulp.src('./web-test/types/*.*'));
        }

        // push app dependencies as TS compiler option paths, to be able to resolve modules of other apps
        var pathsArr = ['*'];
        getAppConfigs().forEach(function(appConfig) {
            if (appConfig.distDependencies) {
                pathsArr = pathsArr.concat(appConfig.distDependencies.map(function(dependency) {
                    return "../" + dependency + "/web/*";
                }));
            }
        });

        var tsResult = series(streams)
            .pipe(typescript.createProject({
                typescript: require('typescript'),
                declaration: false,
                target: 'ES5',
                module: 'amd',
                baseUrl: ".",
                paths: {
                    "*": pathsArr
                },
                types: [''] //don't include the node_modules/@types
            })());
        var result = tsResult.js
            .pipe(gulp.dest(appConfig.bundle + appConfig.output));

        if (returnResult) {
            return result;
        }
    }

    if (WATCH_MODE) {
        var touched = [];
        return gulp.src(getAllSourcesFor('typescript'))
            .pipe(cache('typescript'))
            .pipe(tap(function(file, t) {
                touched.push(file.relative, file.relative);
                delete cache.caches['typescript'][file.path]; //remove from cache, else it won't be compiled when using cache pipeline again
            }))
            .on('end', function() {
                var touchedFiles = touched.map(function(file) {
                    return file.split(path.sep)[0];
                });
                unique(touchedFiles).forEach(function(path) {
                    compileTypescript(getAppConfigByPath(path));
                });
            });
    } else {
        return compileTypescript(checkAppConfig(appConfig), true);
    }

});
