var gulp = require('gulp'),
    gutil = require('gulp-util'),
    tap = require("gulp-tap");

gulp.task('e2eDiscoverConfig', function(done) {
    return gulp.src(['./../*/*.uitest','!./../*/*.uitest/**'])
        .pipe(tap(function(file) {
            E2E_CONFIG = getFile('../' + file.relative);
            E2E_BUNDLE = './../' + extractAppFolderFromPath(file.relative);

            validate(E2E_CONFIG, file.relative, 'e2e');
        }))
        .on('end', function() {
            if (!E2E_CONFIG) {
                gutil.log(gutil.colors.red('No e2e configuration found! Please specify a *.uitest file'));
                process.exit(1);
            }
        });
});
