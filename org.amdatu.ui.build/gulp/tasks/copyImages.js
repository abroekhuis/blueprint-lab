var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    cache = require('gulp-cached');

gulp.task('copyImages', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    return gulp
        .src(gulpif(WATCH_MODE, getAllSourcesFor('images'), prefixSources(appConfig.images.sources, appConfig.bundle + '/')))
        .pipe(cache('images'))
        .pipe(gulpif(WATCH_MODE, gulp.dest(getOutputForFile), gulp.dest(appConfig.bundle + appConfig.output)));
});
