var gulp = require('gulp'),
    consolidate = require('gulp-consolidate'),
    rename = require('gulp-rename'),
    iconfont = require('gulp-iconfont');

gulp.task('genFontFromSvg', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);

    var createCacheStream = function(appConfig) {
        var output = getOutputFolder(appConfig),
            runTimestamp = Math.round(Date.now()/1000);

        return gulp.src(prefixSources(appConfig.fonticon.sources, appConfig.bundle + '/'))
    		.pipe(iconfont({
    			fontName: appConfig.fonticon.fontName,
    			appendCodepoints: true,
                prependUnicode: true,
                timestamp: runTimestamp
    		}))
            .on('glyphs', function(glyphs, options) {
    			var options = {
    				glyphs: glyphs,
    				fontName: appConfig.fonticon.fontName,
                    fontPath: '/' + appConfig.folder + '/' + appConfig.fonticon.fontOutput + '/',
    				className: 'fi'
    			};

    			// generate CSS from template
    			gulp.src(appConfig.bundle + '/' + appConfig.fonticon.templateSources + '/fonticon.css')
    				.pipe(consolidate('lodash', options))
    				.pipe(rename({ basename: 'fonticon', prefix: '_', extname: '.scss' }))
    				.pipe(gulp.dest(appConfig.bundle + '/' + appConfig.fonticon.cssOutput));
    		})
    		.pipe(gulp.dest(output + '/' + appConfig.fonticon.fontOutput));
    }

    if (WATCH_MODE) {
        var streams = [];
        getAppConfigs().forEach(function(currentAppConfig) {
            if (currentAppConfig.fonticon) streams.push(createCacheStream(currentAppConfig));
        });
        (streams.length > 0) ? mergeStreams(streams, done) : done();
    } else {
        return createCacheStream(appConfig);
    }
});
