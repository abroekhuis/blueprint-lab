var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    cache = require('gulp-cached');

gulp.task('copyJson', function(done, appConfig) {
    var appConfig = checkAppConfig(appConfig);
    return gulp
        .src(gulpif(WATCH_MODE, getAllSourcesFor('json'), appConfig.bundle + '/' + appConfig.json.sources))
        .pipe(cache('json'))
        .pipe(gulpif(WATCH_MODE, gulp.dest(getOutputForFile), gulp.dest(appConfig.bundle + appConfig.output)));
});
