var gulp = require('gulp'),
    gutil = require('gulp-util'),
    argv = require('yargs').argv,
    path = require('path'),
    fs = require('fs'),
    request = require('request-json'),
    protractor = require("gulp-protractor").protractor,
    protractorConfigPath = './e2e/protractor.conf.RUN.js',
    browserStackRestAPI = 'https://www.browserstack.com/automate/';

gulp.task('e2eTests', function(done) {
    var env = argv.env ? argv.env : 'local';

    var getConfig = function(env) {
        var config = require(path.resolve( __dirname, "./../../e2e/protractor.conf." + env + ".js" ) ).config;
        if (E2E_CONFIG.protractorConfig && E2E_CONFIG.protractorConfig.local.baseUrl) {
            config.baseUrl = E2E_CONFIG.protractorConfig[env].baseUrl;
            config.params.baseUrl = E2E_CONFIG.protractorConfig[env].baseUrl;
        }
        if (E2E_CONFIG.protractorConfig && E2E_CONFIG.protractorConfig[env].multiCapabilities) {
            config.multiCapabilities = E2E_CONFIG.protractorConfig[env].multiCapabilities;
        }
        if (env === 'browserstack') {
            Object.keys(E2E_CONFIG.suites).forEach(function(suiteName) {
                config.suites[suiteName] = prefixSources(E2E_CONFIG.suites[suiteName], E2E_BUNDLE + '/static/');
            })
        }
        fs.writeFile(protractorConfigPath, 'exports.config = ' + JSON.stringify(config), 'utf8');
    }

    var updateBrowserStackJob = function(e) {
        if (env === 'local') return;
        var isError = e != undefined;
        var user = E2E_CONFIG.protractorConfig[env].multiCapabilities[0]['browserstack.user'];
        var key = E2E_CONFIG.protractorConfig[env].multiCapabilities[0]['browserstack.key'];
        var buildName = E2E_CONFIG.protractorConfig[env].multiCapabilities[0].build;

        var client = request.createClient(browserStackRestAPI);
        client.setBasicAuth(user, key);
        client.get('builds.json', function (error, response, builds) {
            builds.forEach(function(obj) {
                var build = obj['automation_build'];
                if (build.name === buildName) {
                    client.get('builds/' + build.hashed_id + '/sessions.json', function (error, response, sessions) {
                        if (!error && response.statusCode == 200) {
                            var sessionId = sessions[0]['automation_session'].hashed_id;
                            var statusPayload = { "status" : isError ? 'error' : 'completed' };
                            client.put(browserStackRestAPI + 'sessions/' + sessionId + '.json', statusPayload, function() {
                                gutil.log("Updated status of BrowserStack session to " + statusPayload.status);
                                var exitCode = (isError) ? 1 : 0;
                                done();
                                process.exit(exitCode);
                            });
                        }
                    });
                }
            });
        });
    }

    getConfig(env);

    var sources = argv.suite ? E2E_CONFIG.suites[argv.suite] : E2E_CONFIG.suites[Object.keys(E2E_CONFIG.suites)[0]];
    sources = sources.concat(E2E_CONFIG.definitions);

    return gulp.src(prefixSources(sources, E2E_BUNDLE + '/static/'))
        .pipe(protractor({
            configFile: protractorConfigPath,
            debug: false
        })
        .on('error', updateBrowserStackJob)
		.on('end', updateBrowserStackJob)
    );
});
