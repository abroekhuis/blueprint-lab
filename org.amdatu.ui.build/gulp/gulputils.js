var gutil = require('gulp-util'),
    fs = require('fs'),
    path = require('path'),
    gulpTaskMetadata = require('gulp-task-metadata'),
    jsonValidator = require('jsonschema').Validator,
    schema_appconfig = JSON.parse(fs.readFileSync('./gulp/appconfig.schema.json', 'utf-8')),
    schema_e2e = JSON.parse(fs.readFileSync('./gulp/e2econfig.schema.json', 'utf-8'));;

getApps = function() {
    return Object.keys(APPCONFIGS);
}

getAppConfigs = function() {
    return getApps().map(function(app) {
        return APPCONFIGS[app];
    });
}

getAppConfig = function(appName) {
    if (!APPCONFIGS[appName]) {
        gutil.log(gutil.colors.red('Configuration for ' + appName + ' not found. Please check your references in the other .ui config files.'));
        return;
    }
    return APPCONFIGS[appName];
}

getAppConfigByNamePart = function(namepart) {
    return getAppConfigs().filter(function(appConfig) {
        return appConfig.name.indexOf(namepart) > -1;
    })[0];
}

getAppConfigByPath = function(path) {
    return getAppConfigs().filter(function(appConfig) {
        return appConfig.folder.indexOf(path) === 0;
    })[0];
}

checkAppConfig = function(appConfig) {
    return (appConfig) ? appConfig : {'typescript':'', 'sass':'', 'html': '', 'javascript': '', 'json': '','images': '', 'svg': ''};
}

validateAppConfiguration = function(appName) {
    validate(getAppConfig(appName), appName + '.ui', 'appconfig');
}

validate = function(json, filename, schema) {
    var schemaFile = (schema === 'appconfig') ? schema_appconfig : schema_e2e,
        validator = new jsonValidator();

    validator.validate(json, schemaFile, {
        propertyName: 'Configuration file "' + filename + '"',
        throwError: true
    });
}

registerApp = function(path) {
    var appName = extractBundleFromPath(path);
    gulp.series('discoverApps', function(done) {
        cache.caches = {};
        registerAppTasks(appName, true);

        WATCH_MODE = false;
        gutil.log('Configuration added for ' + gutil.colors.green(appName));
        gulp.series(appName+'.build', function(done) {
            WATCH_MODE = true;
            done();
        })();
        done();
    })();
}

unregisterApp = function(path) {
    var bundle = extractBundleFromPath(path);
    gutil.log('Configuration removed for ' + gutil.colors.green(bundle));
    gulpTaskMetadata.series([getAppConfig(bundle)], 'clean','discoverApps')();
}

updateApp = function(path) {
    var appName = extractBundleFromPath(path);
    gutil.log('Configuration updated for ' + gutil.colors.green(appName));
    validateAppConfiguration(appName);
    gulp.series('discoverApps', function(done) {
        cache.caches = {};
        registerAppTasks(appName, true);
        done();
    }, appName+'.build', 'copyMain')();
}

registerAppTasks = function(appName, execute) {
    var appConfig = getAppConfig(appName);
    var tasks = filterTasks(appConfig, ['genTemplateCache','genTranslationCache','compileTS','compileSass','lintTS','copyHtml','copyFonts','copyJavascript','copyJson','copyImages','genStyleguide']);

    gulp.task(appName+'.build',
        gulpTaskMetadata.series([appConfig], 'clean', filterTasks(appConfig, ['genFontFromSvg']),
            gulpTaskMetadata.parallel([appConfig], tasks,
                gulpTaskMetadata.series([appConfig], 'installBowerLibs','copyBowerLibs')
            ),
            filterTasks(appConfig, ['extractTranslations'])
        )
    );
}

filterTasks = function(appConfig, tasks) {
    var configPropsToTasks = {
        'typescript': ['compileTS','lintTS'],
        'sass': ['compileSass'],
        'html': ['genTemplateCache','copyHtml'],
        'json': ['genTranslationCache','copyJson'],
        'javascript': ['copyJavascript'],
        'images': ['copyImages'],
        'fonts': ['copyFonts'],
        'fonticon': ['genFontFromSvg'],
        'styleguide': ['genStyleguide'],
        'extractTranslationsToPot': ['extractTranslations']
    };
    Object.keys(configPropsToTasks).forEach(function(property) {
        if (!appConfig[property] || !appConfig[property].sources) {
            configPropsToTasks[property].forEach(function(value) {
                if (tasks.indexOf(value) > -1) tasks.splice(tasks.indexOf(value), 1);
            });
        }
    });
    return tasks;
}

extractAppFolderFromPath = function(pathstring) {
    return pathstring.split(path.sep).shift();
}

extractBundleFromPath = function(pathstring) {
    return pathstring.split(path.sep).splice(-2,1).shift();
}

getAllSourcesFor = function(configType, property) {
    var sources = [];
    getAppConfigs().forEach(function(appConfig) {
        if (appConfig[configType]) {
            if (property) {
                var propValue = appConfig[configType][property];
                if (Array.isArray(propValue)) {
                    propValue.forEach(function(value) {
                        sources.push((value.startsWith('..') ? './' : appConfig.bundle + '/') + value);
                    });
                } else {
                    sources.push((propValue.startsWith('..') ? './' : appConfig.bundle + '/') + propValue);
                }
            } else {
                appConfig[configType].sources.forEach(function(source) {
                    sources.push(appConfig.bundle + '/' + source);
                });
                sources.push(appConfig.bundle + '/' + appConfig[configType].sources);
                sources.push('!' + appConfig.bundle + appConfig.output + '/*');
            }
        }
    });
    return sources;
}

prefixSources = function(sources, prefix) {
    if (!sources) return sources;
    return sources.map(function(src) {
        return prefix + src;
    });
}

getOutputForFile = function(file) {
    var appConfig = getAppConfigs().filter(function(appConfig) {
        return file.path.indexOf(appConfig.bundle.split('/').pop()) > -1;
    })[0];
    return appConfig.bundle + appConfig.output;
}

getOutputFolder = function(appConfig) {
    return appConfig.bundle + appConfig.output + '/' + appConfig.folder;
}

getFile = function(file) {
    try {
        return JSON.parse(fs.readFileSync(file, "utf8")
            .replace(/([^:]|^)\/\/(.*)$|require\.config\(|\);/gm,'').replace(/\'/g,'"')); //remove comments and require.config call to make it valid json
    } catch (e) {
        console.log('Your file (' + file + ') is probably not valid JSON. Please fix the following syntax error and try again:\n' + e);
        process.exit(-1);
    }
}

mergeStreams = function(streams, done) {
    // we signal done by hand because es.merge.apply(null, streams); doesn't work, see https://github.com/gulpjs/gulp/issues/561
    if (streams.length === 0) return done();
    var count = 0;
    streams.map(function (stream) {
        stream.on('end', function () {
            if (count == (streams.length-1)) done(); else count++;
        });
    });
}
