/**
 * Configuration file for Protractor (E2E test framework) tests against BrowserStack
 */
exports.config = {
    seleniumAddress: 'http://hub.browserstack.com/wd/hub',
    suites: {
        // will be provided by the caller
	},
    baseUrl: '', // will be provided by the caller
    params: {
		baseUrl: '' // will be provided by the caller
	},
	chromeOnly: false,
    maxSessions: 1,
    allScriptsTimeout: 20000,
    getPageTimeout: 15000,
    multiCapabilities: [
        // will be provided by the caller
    ],
    framework: 'jasmine',
    jasmineNodeOpts: {
    	showColors: true,
    	defaultTimeoutInterval: 65000
    },
    onPrepare: function() {
        var jasmineReporters = require('jasmine-reporters');
        var failFast = require('jasmine-fail-fast');

		return browser.getCapabilities().then(function (capabilities) {
			jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
				consolidateAll: true,
				savePath: 'testresults',
				filePrefix: 'xmloutput'
			}));

			jasmine.getEnv().addReporter(failFast.init());

			browser.params.browserName = capabilities.get('browserName');
            browser.params.platform = capabilities.get('platform');

			//increase timeout
			if (browser.params.browserName !== 'safari') {
				browser.driver.manage().timeouts().pageLoadTimeout(75000);
			}
			if (capabilities.get('browser') !== "ipad") {
				browser.driver.manage().window().setSize(1024, 768);
			}
    	});
    },
	onComplete: function(success) {
		if (!success) {
			console.log('\nThere is a test failure, will sleep for 10 seconds, so you can have a look at what might have gone wrong...');
			// Wait a few seconds, so you can see the error, especially handy in a remote execution
			return browser.sleep(10000);
		}
	}
};
