/**
 * Configuration file for Protractor (E2E test framework) tests
 */
exports.config = {
	seleniumServerJar: './../node_modules/protractor/node_modules/webdriver-manager/selenium/selenium-server-standalone-3.3.1.jar',
	params: {
		baseUrl: 'http://localhost:8000/', // default url, will be provided by the caller
		env: 'local'
	},
	chromeOnly: false,
    maxSessions: 1,
    multiCapabilities: [
        {'browserName': 'chrome'} // default capability, will be provided by the caller
    ],
	framework: 'jasmine',
    jasmineNodeOpts: {
        realtimeFailure: true
    },
	onPrepare: function() {
		var jasmineReporters = require('jasmine-reporters');
		var failFast = require('jasmine-fail-fast');

		browser.driver.manage().window().setSize(1024, 768);

		return browser.getCapabilities().then(function (capabilities) {
			jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
				consolidateAll: true,
				savePath: 'testresults',
				filePrefix: 'xmloutput'
			}));

			jasmine.getEnv().addReporter(failFast.init());

			browser.params.browserName = capabilities.get('browserName');
			browser.params.platform = capabilities.get('platform');

			if (browser.params.browserName !== 'safari') {
				browser.driver.manage().timeouts().pageLoadTimeout(75000);
			}


    	});
    },
	onComplete: function(success) {
		if (!success) {
			console.log('\nThere is a test failure, will sleep for 10 seconds, so you can have a look at what might have gone wrong...');
			// Wait a few seconds, so you can see the error, especially handy in a remote execution
			return browser.sleep(10000);
		}
	}
}
