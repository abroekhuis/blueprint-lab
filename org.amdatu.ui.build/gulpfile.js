// global variables
APPCONFIGS = {}, APPCONFIG = '', E2E_CONFIG = '', E2E_BUNDLE = '';
WATCH_MODE = false;

//require needed node modules
var HubRegistry = require('gulp-hub'),
    gutil = require('gulp-util'),
    hub = new HubRegistry(['gulp/tasks/*.js']),
    utils = require('./gulp/gulputils'),
    watchInterval = { interval: 150 },
    argv = require('yargs').argv;

cache = require('gulp-cached'),
gulp = require('gulp');
gulp.registry(hub);

gulp.task('watch', function(done) {
    gulp.watch(getAllSourcesFor('sass'), watchInterval, gulp.series('compileSass'));
    gulp.watch(getAllSourcesFor('typescript'), watchInterval, gulp.series('compileTS','lintTS','copyMain','extractTranslations','unitTests'));
    gulp.watch(getAllSourcesFor('html'), watchInterval, gulp.series('copyHtml','extractTranslations','genTemplateCache'));
    gulp.watch(getAllSourcesFor('javascript'), watchInterval, gulp.series('copyJavascript','extractTranslations'));
    gulp.watch(getAllSourcesFor('json'), watchInterval, gulp.series('copyJson','genTranslationCache'));
    gulp.watch(getAllSourcesFor('images'), watchInterval, gulp.series('copyImages'));
    gulp.watch(getAllSourcesFor('fonts'), watchInterval, gulp.series('copyFonts'));
    gulp.watch(getAllSourcesFor('fonticon'), watchInterval, gulp.series('genFontFromSvg','compileSass'));
    gulp.watch(['./../*/*.ui','!./../*/*.ui/**'], watchInterval)
        .on('add', registerApp)
        .on('change', updateApp)
        .on('unlink', unregisterApp);
    WATCH_MODE = true;
    done();
});

gulp.task('build', function(done) {
    gulp.series(gulp.parallel(getApps().map(function(app) {
        return app + '.build';
    })), 'copyMain', 'unitTests', 'serve', 'watch')();
    done();
});

gulp.task('dist', gulp.series('discoverApps', function (done) {
    gulp.series(gulp.parallel(getApps().map(function(app) {
        return app + '.build';
    })), function(done) {
        gutil.log(gutil.colors.green('Distributing all apps...'));
        done();
    }, 'copyMain', 'unitTests', 'distScripts', 'replaceConstants', 'replaceRev', 'injectDeps', 'cleanDist')();
    done();
}));

gulp.task('cleanup', gulp.series('discoverApps', 'clean'));
gulp.task('webserver', gulp.series('discoverApps', 'serve'));
gulp.task('e2e', gulp.series('e2eDiscoverConfig', gulp.parallel('e2eWebdriverUpdate', 'e2eClean'), 'e2eCopy', 'e2eCompileTS', 'e2eTests'));

gulp.task('default', function(done) {
    cache.caches = {};
    if (argv.app) { //build specified app only
        gulp.series('discoverApps', function(done) {
            var appConf = getAppConfigByPath(argv.app);
            if (!appConf) gutil.log(gutil.colors.red('No app configuration found for ' + argv.app));
            else gulp.series(appConf.name + '.build')();
            done();
        })();
    } else { // build all apps
        gulp.series('discoverApps', 'build')();
    }
    done();
});
