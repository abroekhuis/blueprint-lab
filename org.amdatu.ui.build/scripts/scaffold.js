'use strict';

// IMPORTS
var chalk = require('chalk'),
    path = require('path'),
    fs = require('fs'),
    util = require(path.resolve(__dirname, "./util.js")),
    prompt = require('prompt-sync')({ sigint: true });

// VARIABLES
var config = {},
    prefix = '✍️  ',
    runconfig = util.fromDir('../','.uirun');

// ASK CONFIGURATION
if (runconfig && runconfig.defaultTemplateBundle) {
    config.templateBundleName = runconfig.defaultTemplateBundle;
} else {
    config.templateBundleName = prompt(prefix + chalk.bold.cyan('Specify the bundle name of your app template to use for scaffolding. '), '');
}
util.validateNotEmpty(config.templateBundleName, 'You must specify a template bundle name!');
util.validateIsDir('../' + config.templateBundleName, 'Directory "' + config.templateBundleName + '" not found!');
config.appName = prompt(prefix + chalk.bold.cyan('Specify the name of the new app '), '');
util.validateNotEmpty(config.appName, 'You must specify an app name!');
config.appNameCamelCase = util.camelize(config.appName);
config.appNamePascalCase = config.appNameCamelCase.charAt(0).toUpperCase() + config.appNameCamelCase.slice(1);
config.appNameLowerCase = config.appNameCamelCase.toLowerCase();
config.appNameUpperCase = config.appNameCamelCase.toUpperCase();
config.targetBundleName = prompt(prefix + chalk.bold.cyan('Specify the target bundle name to scaffold the new app into '), '');
util.validateNotEmpty(config.targetBundleName, 'You must specify a target bundle name!');
config.targetBundlePath = '../' + config.targetBundleName + '/';
util.validateIsDir(config.targetBundlePath, 'Directory "' + config.targetBundlePath + '" not found!');
config.copyJavaSources = prompt(prefix + chalk.bold.cyan('Do you want to copy the Java resources (packages, .bnd file) also? (y/n) '), 'yes');
config.dependOnSharedBundle = prompt(prefix + chalk.bold.cyan('Do you want to depend on another project with shared code? (y/n) '), 'no');
if (util.isTruthy(config.dependOnSharedBundle)) {
    config.sharedBundle = prompt(prefix + chalk.bold.cyan('Specify the bundle name you want to depend the new app on '), '');
}

var searchArr = [/SAMPLE/g, /Sample/g, /sample/g, /\.ui/g, /\$sharedBundle\$/g, /\$targetBundle\$/g];
var replaceArr = [config.appNameUpperCase, config.appNamePascalCase, config.appNameLowerCase, '.ui', config.sharedBundle, config.targetBundleName];
var confFile = config.sharedBundle ? 'sample.ui.shared' : 'sample.ui';

// SCAFFOLD APP
try {
    util.copyAndReplace('../' + config.templateBundleName + '/web/sample', config.targetBundlePath + 'web/' + config.appNameLowerCase, searchArr, replaceArr);
    util.copyAndReplace('../' + config.templateBundleName + '/' + confFile, config.targetBundlePath + config.appNameLowerCase + '.uidisabled', searchArr, replaceArr);
    if (!config.sharedBundle) {
        util.copy('../' + config.templateBundleName + '/bower.json', config.targetBundlePath + 'bower.json');
        util.copy('../' + config.templateBundleName + '/types', config.targetBundlePath + 'types');
    } else {
        util.remove(config.targetBundlePath + 'web/' + config.appNameLowerCase + '/main.ts');
    }
    if (util.isTruthy(config.copyJavaSources)) {
        util.copyJavaPackages('../' + config.templateBundleName + '/src', config.targetBundlePath + 'src', config.targetBundleName, searchArr, replaceArr);
        util.copyAndReplace('../' + config.templateBundleName + '/bnd.bnd', config.targetBundlePath + 'bnd.bnd', searchArr, replaceArr);
    }

    console.log(chalk.bold.green('✅  App "' + config.appName + '" scaffolded!'));
    console.log(chalk.bold.white('Start Modular UI if it\'s not running yet to see it working.'));
} catch (error) {
    console.log(chalk.bold.red('⛔  Whoops, something went wrong during scaffolding the new app! Please check your entered target bundle name.'), error);
}
