'use strict';

var fs = require('fs-extra'),
    replace = require('replace'),
    path = require('path'),
    chalk = require('chalk');

module.exports = function () {
    function camelize(str) {
        return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
            if (+match === 0) return '';
            return index == 0 ? match.toLowerCase() : match.toUpperCase();
        });
    }

    function isTruthy(answer) {
        return (answer == 'y') || (answer == 'yes');
    }

    function logError(msg, exit) {
        console.log(chalk.bold.red(msg));
        if (exit) process.exit(0);
    }

    function validateNotEmpty(configvar, msg) {
        if (!configvar) logError(msg, true);
    }

    function validateIsDir(dir, msg) {
        try {
            if (!fs.lstatSync(dir).isDirectory()) logError('⛔ ' + msg, true);
        } catch(error) {
            logError('⛔  ' + msg, true);
        };
    }

    function renameFilesRecursive(dir, from, to) {
        if (!fs.lstatSync(dir).isDirectory()) return;

        fs.readdirSync(dir).forEach(it => {
            var itsPath = path.resolve(dir, it),
                itsStat = fs.statSync(itsPath);

            if (itsStat.isDirectory()) {
                itsPath = rename(itsPath, from, to);
                renameFilesRecursive(itsPath, from, to)
            } else {
                rename(itsPath, from, to);
            }
        })
    }

    function rename(path, from, to) {
        from.forEach(function(fromPart, index) {
            if (path.search(fromPart) != -1) {
                var newPath = path.replace(fromPart, to[index]);
                fs.renameSync(path, newPath)
                path = newPath;
            }
        })
        return path;
    }

    function copy(source, target) {
        if (fs.existsSync(source)) {
            fs.copySync(source, target);
        }
    }

    /**
     * Copy packages and rename them upfront to keep the nested directory structure
     */
    function copyJavaPackages(source, target, targetPackage, searchArr, replaceArr) {
        targetPackage = targetPackage.replace(/\./g,'/');
        fs.readdirSync(source).forEach(subdir => {
            var targetPackagePath = subdir.replace(/\$targetBundle\$/g, targetPackage);
            copyAndReplace(source + '/' + subdir, target + '/' + targetPackagePath, searchArr, replaceArr);
        });
    }

    function copyAndReplace(source, target, searchArr, replaceArr) {
        copy(source, target);
        if (!fs.existsSync(target)) return;

        // rename sample occurrences inside files and in file names
        searchArr.forEach(function(search, index) {
            replace({
                regex: search,
                replacement: replaceArr[index],
                paths: [target],
                recursive: true,
                silent: true
            });
        })
        renameFilesRecursive(target, searchArr, replaceArr);

        if (target.indexOf('.uidisabled') > -1) {
            setTimeout(function() { //wait a bit until all files are renamed
                fs.renameSync(target, target.replace('.uidisabled', '.ui'))
            }, 750);
        }
    }

    function remove(path) {
        fs.removeSync(path);
    }

    function fromDir(startPath,filter) {
        var files = fs.readdirSync(startPath),
            found;

        for (var file of files) {
            var filename= path.join(startPath,file);
            var stat = fs.lstatSync(filename);
            if (stat.isDirectory()) {
                found = fromDir(filename, filter); //recurse
                if (found) break;
            }
            else if (filename.indexOf(filter) >= 0) {
                found = JSON.parse(fs.readFileSync(filename, "utf8"));
                break;
            }
        }
        return found;
    }

    return {
        camelize: camelize,
        isTruthy: isTruthy,
        validateNotEmpty: validateNotEmpty,
        validateIsDir: validateIsDir,
        copy: copy,
        copyJavaPackages: copyJavaPackages,
        copyAndReplace: copyAndReplace,
        remove: remove,
        fromDir: fromDir
    }
}();
